<!-- @format -->

<p align="center">
   <img src="assets/ts.png" width="100">
</p>

# Introduction

## TypeScript vs JavaScript

- Angular is written in TypeScript
- TypeScript = ES6 Javascript & Extras

<p align="center">
   <img src="assets/venn-es5-es6-typescript.png">
</p>

## Transpilation

- Browsers don’t support TypeScript.
- Browsers barely support ES6 JavaScript
- We can write in TypeScript and have a **transpiler** convert to ES6 or ES5.
- Since most browsers don’t support ES6 features yet we are going to transpile our TypeScript into ES5.

```json
{
  "compilerOptions": {
    "target": "es5", (1)
    "module": "commonjs",
    "moduleResolution": "node",
    "sourceMap": true,
    "emitDecoratorMetadata": true,
    "experimentalDecorators": true,
    "removeComments": false,
    "noImplicitAny": true,
    "suppressImplicitAnyIndexErrors": true
  }
}
```

## Summary

- TypeScript is just JavaScript with a few more advanced features.

- Browser can’t run TypeScript so we first need to transpile it into JavaScript.

- The most common version of JavaScript is currently ES5 so we transpile TypeScript into ES5 JavaScript.

# Installation

- Browsers don’t know about to convert TypeScript code
- For converting we use **_tsc_** to compile on the command line.
- But it’s not necessary to do this in your everyday Angular development.
- **_webpack bundler_** do this job on angular app

```
npm install -g typescript
```

- Make sure everything worked correctly

```
tsc –v
```

## Running TypeScript

Create a file called **hello.ts**.

We can compile a typescript file into a javascript file by calling:

```
tsc hello.ts
```

This generates a file called **hello.js**

And we can execute that file by using node, like so:

```
node hello.js
```

We can watch a typescript file for changes and compile it automatically with:

```
tsc -w hello.ts
```

We can provide configuration on the command line, like so:

```
tsc -t ES6 -w hello.ts
```

> The above -t ES6 is a flag to tell typescript to transpile into ES6 instead of the default ES5.

We can create a config file with the most common settings like so:

```
tsc --init
```

If we want to watch all files in a directory we can just type:

```
tsc -w
```

## Workouts

Write a program in typescript and console ouput

- Print a string from a variable
- Do all Arthmetic operations
- Do all Arthmetic operations using function
- Add all elemnts in an array
- Find lenght of string
- Search string in a string
- Replace string in a string
- Convert string to Upper and Lower Case
- Make a car object have properties **_name,model,weight,color_** and methods **_start, drive, brake,stop_**

# Block Scope

Scope refers to the lifecycle of a variable

In Java and C++ there is the concept of block scope, a block is any code that is wrapped in **{** and **}**, like so:

```javascript
{
  // This is a block
}
// This is not part a block
```

But in ES5 JavaScript we only have two scopes, the ****global scope**** and ****function scope****.

Eg: ****global scope****

legal in javascript not in other languages

```javascript
{
  var a = "block";
}
console.log(a);
```

Eg: ****function scope****

```javascript
function hello() {
  var a = "function";
}
hello();
console.log(a);
```

If we ran the above we would get an error, like so:

```
Uncaught ReferenceError: a is not defined(…)
```

because variable scope only in function

Eg:

```javascript
function hello() {
  var a = "function";
  for (var i = 0; i < 10; i++) {
    var a = "block";
  }
  console.log(a);
}
hello();
```

> Expect result

```javascript
function hello() {
  var a = "function";

  for (var i = 0; i < 5; i++) {
    (function() {
      //
      var a = "block"; // Describe
    })(); //
  }
  console.log(a);
}
hello();
```

> Expect result

## Let

- ES6 we now have the new **let** keyword, we use it in place of the **var** keyword and it creates a variable with block level scope

Eg:

```javascript
function hello() {
  var a = "function";
  for (var i = 0; i < 5; i++) {
    let a = "block"; // changed to var to let
  }
  console.log(a);
}
hello();
```

## Summary

- **let** is a very important addition the javascript language in ES6.

- It’s not a replacement to **var**, **var** can still be used even in ES6 and has the same semantics as ES5.

- However unless you have a specific reason to use **var** I would expect all variables you define from now on to use let.

# Const

- **const** variable must be initilized (show error withou init)
- **cont** variable not changed after init

- **const** variable is block scoped like **let**

```javascript
function func() {
  if (true) {
    const tmp = 123;
  }
  console.log(tmp); // Uncaught ReferenceError: tmp is not defined
}
func();
```

## Immutable variable

- Variables created by let and var are mutable:

```javascript
let foo = "foo";
foo = "moo";
console.log(foo);
```

- **const** variables are immutable

- But we can however mutate, make changes to, the object foo points to, like so:

```javascript
const foo = {};
foo["prop"] = "Moo"; // This works!
console.log(foo);
```

```javascript
const foo = Object.freeze({});
foo.prop = 123;
console.log(foo.prop); // undefined
```

```javascript
"use strict";
const foo = Object.freeze({});
foo.prop = 123; // SyntaxError: Identifier 'foo' has already been declared
```

# Template Strings

- With ES5 and ES6 we can specify a string with either the **'** or **"** characters.

```javascript
let single = "hello world";
```

## ES5 multiline string

Single line

```javascript
let single = 'hello ' +
    'iam ' +
    'learning ' +
    'javascript ' +
```

For print mutli line

```javascript
let multi = 'hello\n' +
    'iam\n' +
    'learning\n' +
    'javascript\n' +
```

## ES6 multiline string

* In ES6 we have another way of defining strings, using the back-tick character **`**

```javascript
let single = `hello
    iam
    learning
    javascript`;
```

## Variable Substitution


```javascript
let technology = "javascript";

let single = `hello
    iam
    learning
    ${technology}`;
```

## Array Program

* Create a string **_JSTSP_** means **_Javascript and Typescript Programing_**
* Make a charector array of given string
* And add fullname of each charector like abow
* And push missing string to given array
* Finally change **_prgramming_** to  **_are Aowsome'_**
* And convert the entire array to string


## Angular Basic App Structure

<p align="center">
   <img src="assets/structure.png" width="300">
</p>


## Converting a JSON data to FORM data

```javascript

  public createFormData(object: Object, form?: FormData, namespace?: string): FormData {
      const formData = form || new FormData();
      for (let property in object) {
          if (!object.hasOwnProperty(property) || !object[property]) {
              continue;
          }
          const formKey = namespace ? `${namespace}[${property}]` : property;
          if (object[property] instanceof Date) {
              formData.append(formKey, object[property].toISOString());
          } else if (typeof object[property] === 'object' && !(object[property] instanceof File)) {
              this.createFormData(object[property], formData, formKey);
          } else {
              formData.append(formKey, object[property]);
          }
      }
      return formData;
  }

```

## GET and SET Image From FormControl

```html
  <input type="file" accept="image" (change)="getImage($event)" class="form-control input-sm" id="fileInput" formControlName="image">
```

```javascript
  getImage(event) {
    let file: File = event.target.files[0]
    this.setImage(file);
    (<HTMLInputElement>document.getElementById('fileInput')).value = null;
  }

  setImage(file) {
    if (file instanceof File) {
      let image: any = {}
      let mime = file.type.startsWith('image/')
      mime = mime ? file.type.endsWith('svg+xml') ? false : true : false
      if (mime) {
        let reader = new FileReader();
        reader.onload = (event: any) => {
          image.image = event.target.result
        }
        reader.readAsDataURL(file);
      }
      this.selImage = image;
      this.b.formGroup.get('image').setValue(file);
    }
  }

```

# Ngx Datatable CSS

```css
@import '~@swimlane/ngx-datatable/index.css';
@import '~@swimlane/ngx-datatable/assets/icons.css';

.datatable-header {
  width: auto !important;
  height: 43px !important;
  background-color: #e9ecef;
  margin: 0 !important;

  border-bottom: 3px solid #dee2e6;
  border-top: 1px solid #dee2e6;
  border-right: 1px solid #dee2e6;
}

.datatable-header .datatable-header-cell {
  background-color: #e9ecef;
  color: #495057;
  border-color: #dee2e6;
  border-left: 1px solid #dee2e6;
  padding: 0.5rem 1.2rem !important;
  border-top: 0;
  font-weight: 600;
  line-height: 2 !important;
}

.datatable-header .datatable-header-cell .sort-btn {
  font-size: 20px;
  float: right;
  margin-top: 4px
}

.datatable-body {
  width: auto !important;
}

.datatable-body-row {
  height: 42px !important;
  border-right: 1px solid #dee2e6;
  border-bottom: 1px solid #dee2e6;
}

.datatable-body-row .datatable-body-cell {
  height: 42px !important;
  padding: 0.5rem 1.2rem !important;
  border-left: 1px solid #dee2e6;
}

.datatable-header-cell-wrapper {
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
```

## DataTable Columns

```javascript
 this.columns = [
      { prop: 'id', width: 50, canAutoResize: false },
      { prop: 'name', width: 300 },
      { name: 'Action', width: 100, canAutoResize: false, resizeable: false, cellTemplate: this.action.buttons },
    ];
```

## Databale Component Options
```html
<ngx-datatable class="default" 
   [rows]="rows" 
   [columns]="columns" 
   [columnMode]="'force'"  
   [headerHeight]="50" 
   [rowHeight]="'auto'" 
   [footerHeight]="50"
   [scrollbarH]="true" 
   [reorderable]="false">
</ngx-datatable>
```
