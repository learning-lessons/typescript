class JSTSP {

    input: string;
    result: string;

    private charectorInputArray: string[];
    private charectorResultArray: string[];

    constructor(input: string, result: string) {
        this.input = input;
        this.result = result;
        console.log('\n// Create a string JSTSP means Javascript and Typescript Programing\n');
        console.log({input: this.input, result: this.result});
    }

    makeCharectorArray() {
        this.charectorInputArray = this.input.split('');
        this.charectorResultArray = this.result.split(' ');
        console.log('\n// Make a charector array of given string\n');
        console.log(this.charectorInputArray);
    }

    addFullName() {
       this.splitScriptFromCharectorResultArray();

       this.charectorInputArray = this.charectorInputArray.map(charectorInput => {
           let fullName = this.charectorResultArray.filter(charectorResult => {
               return charectorResult[0] == charectorInput;
           })[0];

           return fullName;
       });

       console.log('\n// And add fullname of each charector like abow\n');
       console.log(this.charectorInputArray);
    }

    pushMissingString() {
      this.charectorInputArray.splice(2, 0, "And");
      console.log('\n// And push missing string to given array\n');
      console.log(this.charectorInputArray);
    }

    replaceString(replaceStr: string) {
        this.charectorInputArray = this.charectorInputArray.map(charectorInput => {
            return charectorInput == 'Programming' ? replaceStr : charectorInput;
        });
        console.log('\n// Finally change prgramming to  are Aowsome\n');
        console.log(this.charectorInputArray);
    }

    private splitScriptFromCharectorResultArray() {
       let newScriptSplittedArray: string[] = [];

       this.charectorResultArray.forEach((element: string) => {
          if(element.indexOf('script') !== -1) {
            let splittedArray = element.split('script');
            splittedArray[1] = 'Script';
            newScriptSplittedArray = [...newScriptSplittedArray, ...splittedArray];
        } else
            newScriptSplittedArray = [...newScriptSplittedArray, element];
        });
        
        this.charectorResultArray = newScriptSplittedArray;
    }
}

let jstsp = new JSTSP('JSTSP', 'Javascript and Typescript Programming');
jstsp.makeCharectorArray();
jstsp.addFullName();
jstsp.pushMissingString();
jstsp.replaceString('Are Aowsome');
